#! /usr/bin/env bash

pot="$HOME/Katie/translations/qt.pot"

if [ ! -e "$pot" ]; then
    echo "Could not find the Katie pot"
    exit 1
elif ! type -p msgmerge ;then
    echo "msgmerge is not in your PATH"
    exit 1
elif ! type -p msgattrib ;then
    echo "msgattrib is not in your PATH"
    exit 1
fi

name="kdeqt.po"
for p in $(find kde-l10n/ -name "$name");do
    echo "Updating $p..."
    msgmerge --update --no-fuzzy-matching "$p" "$pot"
    echo "Cleaning up $p..."
    msgattrib --no-obsolete "$p" -o "$p"
done
